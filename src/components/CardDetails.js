import React from "react"

import { Link } from "react-router-dom"

import ChecklistContainer from "./ChecklistContainer"

import "./styles.css"
const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"


class CardDetails extends React.Component {
    constructor() {
        super()

        this.state = {
            boardId: 0,
            cardId: 0,
            cardTitle: "",
            cardDesc: "",
            descEdit: false
        }
    }

    componentDidMount() {
        const cardId = document.location.pathname.split("/")[3]
        const boardId = document.location.pathname.split("/")[2]
        console.log(boardId)
        console.log(cardId)
        this.setState(() => {
            return {
                boardId: boardId,
                cardId: cardId,
            }
        })
        fetch(`https://api.trello.com/1/cards/${cardId}?${keyToken}`)
            .then((response) => {
                return (response.json())
            })
            .then((data) => {
                // console.log(data["desc"])
                this.setState(() => {
                    return {
                        cardTitle: data["name"],
                        cardDesc: (data["desc"] === "" ? "Add a more detailed description" : data["desc"])
                    }
                })

            })
    }

    enableEditDesc = () => {
        this.setState((prevState) => {
            return {
                descEdit: (!prevState.descEdit),
                cardDesc: (prevState.cardDesc === "Add a more detailed description" ? "" : prevState.cardDesc)
            }
        })
    }

    editDesc = (event) => {
        this.setState(() => {
            return {
                cardDesc: event.target.value
            }
        })
    }

    render() {
        return (
            <div class="modal-background">
                <div class="modal">
                    <Link to={`/boards/${this.state.boardId}`}><button class="close-card-details-wrapper">x</button></Link>
                    <h2 id="card-details-title">{this.state.cardTitle}</h2>
                    <div id="desc-title-wrapper">
                        <h3 id="desc">Description</h3>
                        <button onClick={this.enableEditDesc}>Edit</button>
                    </div>
                    {this.state.descEdit ? null : <pre class="card-details-desc" placeholder="Add a more detailed description" markdown="1">{this.state.cardDesc}</pre>}
                    {this.state.descEdit ? <textarea class="desc-textarea" rows="20" cols="70" onChange={this.editDesc} value={this.state.cardDesc} /> : null}
                    <ChecklistContainer cardId={this.state.cardId} />
                </div>

            </div>

        )
    }
}

export default CardDetails