import React from "react"
import List from "./List"
import { Link } from "react-router-dom"

import "./styles.css"
const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"

class ListContainer extends React.Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        const boardId = document.location.pathname.split("/")[2]
        // console.log(boardId)

        fetch(`https://api.trello.com/1/boards/${boardId}/lists?${keyToken}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {

                this.setState(() => {
                    return {
                        data: data
                    }
                })
            })
    }

    onCardDisplay = () => {
        this.props.onCardDisplay()
    }

    render() {
        const listElement = this.state.data.map((item) => {
            return (<List cardDisplay={this.onCardDisplay} key={item.id} name={item.name} dataId={item.id} />)
        })
        return (
            <div class="lists-container">
                {listElement}
            </div>
        )
    }
}

export default ListContainer