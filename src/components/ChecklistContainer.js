import React from "react"

import Checklist from "./Checklist"
const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"

class ChecklistContainer extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            checklists: [],
            cardId: this.props.cardId
        }
    }
    componentDidMount() {
        // console.log(this.state)
        const cardId = document.location.pathname.split("/")[3]
        console.log(cardId)
        fetch(`https://api.trello.com/1/cards/${cardId}/checklists?${keyToken}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                const sortedChecklist = data.sort((obj1, obj2) => {
                    if (obj1["pos"] > obj2["pos"]) {
                        return 1
                    }
                    else {
                        return -1
                    }
                })
                console.log(sortedChecklist)

                this.setState(() => {
                    return {
                        checklists: sortedChecklist
                    }
                })
            })
            .catch((error) => {
                console.error(error)
            })

    }
    render() {
        const checklists = this.state.checklists.map((item) => {
            return (<Checklist key={item.id} name={item.name} checklistId={item.id} />)
        })
        return (
            <div>
                {checklists}
            </div>
        )
    }
}

export default ChecklistContainer