import React from "react"

import BoardHeader from "./BoardHeader"
import ListsContainer from "./ListsContainer"
import CardDetails from "./CardDetails"


const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"


class BoardsPage extends React.Component {
    constructor() {
        super()

        this.state = {
            display: false,
            boardName: ""
        }
    }

    componentDidMount() {
        const boardId = document.location.pathname.split("/")[2]
        fetch(`https://api.trello.com/1/boards/${boardId}?${keyToken}`)
            .then((response) => {
                return (response.json())
            })
            .then((data) => {
                console.log(data.name)
                this.setState(() => {
                    return {
                        boardName: data.name
                    }
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        console.log(this.state.boardName)
        return (
            <div class="boards-page">
                <BoardHeader />
                <h2 id="board-name">{this.state.boardName}</h2>
                <ListsContainer onCardDisplay={this.cardDisplay} />
            </div >
        )
    }
}

export default BoardsPage