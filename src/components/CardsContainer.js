import React from "react"
import { Link } from "react-router-dom"

import Card from "./Card"

import "./styles.css"
const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"
class CardsContainer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            listId: this.props.listId,
            data: [],
            newCardName: ""
        }
    }

    componentDidMount() {
        const listId = this.state.listId
        const boardId = document.location.pathname.split("/")[2]
        // console.log(listId)
        fetch(`https://api.trello.com/1/lists/${listId}/cards?${keyToken}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                this.setState(() => {
                    return {
                        data: data,
                        displayAddCard: false
                    }
                })
            })
    }

    cardDisplay = () => {
        console.log(true)
        this.props.onCardDisplay(true)
    }

    enableAddCard = () => {
        this.setState((prevState) => {
            return {
                displayAddCard: (!prevState.displayAddCard)
            }
        })
    }

    closeAddCardTextarea = () => {
        this.setState((prevState) => {
            return {
                displayAddCard: (!prevState.displayAddCard)
            }
        })
    }

    addNewCard = () => {
        this.setState((prevState) => {
            console.log(this)
            return {
                data: (prevState.data.concat(<Link to={""} style={{ textDecoration: 'none' }}><Card onCardDisplay={this.cardDisplay} name={this.state.newCardName} /></Link>)),
            }
        })
        this.setState(() => {
            return {
                newCardName: ""
            }
        })

    }

    newCardName = (event) => {
        this.setState(() => {
            return {
                newCardName: event.target.value
            }
        })
    }

    render() {
        const boardId = document.location.pathname.split("/")[2]

        const cardsEle = this.state.data.map((item) => {
            return (<Link to={`/boards/${boardId}/${item.id}`} style={{ textDecoration: 'none' }}><Card onCardDisplay={this.cardDisplay} key={item.id} dataId={item.id} name={item.name} /></Link>)
        })
        return (
            <div class="cards-container">
                {cardsEle}
                {this.state.displayAddCard ? <textarea rows="3" cols="30" placeholder="Enter a title for card...." id="add-new-card" onChange={this.newCardName}></textarea> : <p id="add-another-card" onClick={this.enableAddCard}>+ add another Card</p>}
                {this.state.displayAddCard ? <div class="add-card-buttons-wrapper"><button id="add-card-button" onClick={this.addNewCard}>Add card</button> <button id="close-add-card-textbox" onClick={this.closeAddCardTextarea}>x</button></div> : null}
            </div>
        )
    }
}

export default CardsContainer