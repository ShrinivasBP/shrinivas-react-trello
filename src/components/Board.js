import React from "react"

import "./styles.css"
class Board extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            boardName: props.boardName,
            id: props.dataId
            // selectedBoard: 0
        }
    }

    displayBoard = () => {
        let selectedBoard = this.state.id
        // this.props.onSelectBoard(selectedBoard)
    }

    render() {
        return (
            <div onClick={this.displayBoard} class="boards-wrapper">
                <h3 id="board-title">{this.state.boardName}</h3>
            </div>
        )
    }
}

export default Board