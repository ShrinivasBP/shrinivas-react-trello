import React from "react"
import { Link } from "react-router-dom"
import Board from "./Board"

import "./styles.css"
const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"
class BoardsContainer extends React.Component {
    constructor() {
        super()

        this.state = {
            data: [],
            board: 0
        }
    }

    componentDidMount() {
        fetch(`https://api.trello.com/1/members/me/boards?fields=name,url&${keyToken}`, {
            method: "GET", headers: {
                'Accept': 'application/json'
            }
        })

            .then((response) => {
                return response.json()
            })
            .then((data) => {


                this.setState(() => {
                    return {
                        data: data
                    }
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }


    render() {
        const boards = this.state.data.map((item) => {
            return (<Link class="board" to={`/boards/${item.id}`}><Board key={item.id} dataId={item.id} boardName={item.name} /></Link>)
        })
        return (
            <div class="home-sec-container">
                {boards}
            </div>
        )
    }
}


export default BoardsContainer