import React from "react"

import CheckItem from "./CheckItem"

const keyToken = "key=e0d433b50454859a6255e15a1add1e9d&token=4ce4bac1f6d724de0c484dc2b7deef288c800a29dcacca5654592045bf5243e7"

class Checklist extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            cardId: this.props.cardId,
            checklistId: this.props.checklistId,
            checkItemId: this.props.checkItemId,
            checklistTitle: this.props.name,
            checkItems: []
        }
    }
    componentDidMount() {
        fetch(`https://api.trello.com/1/checklists/${this.state.checklistId}/checkItems?${keyToken}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                const sortedCheckItems = data.sort((obj1, obj2) => {
                    if (obj1["pos"] > obj2["pos"]) {
                        return 1
                    }
                    else {
                        return -1
                    }
                })
                const checkItems = sortedCheckItems.map((item) => {
                    return (<CheckItem checkItemId={item.id} checked={item.state} checkItemDesc={item.name} />)
                })
                this.setState(() => {
                    return {
                        checkItems: checkItems
                    }
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }
    render() {
        return (
            <div class="checklist-wrapper">
                <h2>{this.state.checklistTitle}</h2>
                <hr></hr>
                {this.state.checkItems}
            </div>
        )
    }
}

export default Checklist