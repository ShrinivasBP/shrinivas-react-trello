import React from "react"

import CardsContainer from "./CardsContainer"

import "./styles.css"
class List extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            list: this.props.name,
            id: this.props.dataId
        }
    }
    cardDisplay = () => {
        console.log(true)
        this.props.cardDisplay(true)
    }
    render() {
        return (
            <div class="list-wrapper">
                <h4 id="list-title">{this.state.list}</h4>
                <CardsContainer onCardDisplay={this.cardDisplay} listId={this.state.id} title={this.state.list} />
            </div >
        )
    }
}

export default List