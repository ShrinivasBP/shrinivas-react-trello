import React from "react"

import "./styles.css"
class Card extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            card: this.props.name,
            cardId: this.props.id,
            display: false
        }
    }

    displayCard = () => {
        this.setState((prevState) => {
            return {
                display: !(prevState.display)
            }
        })
    }

    render() {
        return (
            <div>
                <div class="card-wrapper">
                    <p id="card-title">{this.state.card}</p>
                    <button id="card-close-button">x</button>
                </div>

            </div>
        )
    }
}

export default Card