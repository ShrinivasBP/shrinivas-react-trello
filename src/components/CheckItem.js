import React from "react"

class CheckItem extends React.Component {
    constructor(props) {
        super(props)

        this.state = {

            checkItemId: this.props.checkItemId,
            checkItemDesc: this.props.checkItemDesc,
            checked: this.props.checked === "complete" ? true : false
        }

    }
    changeCheckbox = () => {
        this.setState((prevState) => {
            return {
                checked: (!prevState.checked)
            }
        })
    }
    render() {
        const textStyle = {
            color: "grey",
            textDecoration: "line-through"
        }

        return (
            <div class="check-item-wrapper">
                <input onClick={this.changeCheckbox} checked={this.state.checked} type="checkbox"></input>
                <p style={this.state.checked ? textStyle : null} >{this.state.checkItemDesc}</p>
            </div>
        )
    }
}

export default CheckItem