import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Header from "./components/Header"
import BoardsContainer from "./components/BoardsContainer"
import CardDetails from "./components/CardDetails"
import BoardsPage from "./components/BoardsPage"
import './App.css';

class App extends React.Component {
  constructor() {
    super()

    this.state = {
      board: 0,
      CardDetails: false
    }
  }

  receiveBoardId = (boardId) => {
    console.log(boardId)
  }

  render() {
    return (
      <Router>
        <div className="App" >
          <Switch>

            <Route path="/" exact component={Header} />
            <Route path="/boards" exact component={CollectionOfBoardsPage} />
            <Route path="/boards/:boardId" exact component={BoardsPage} />
            <Route path="/boards/:boardId/:cardId" component={card} />

          </Switch>
        </div>
      </Router>
    )
  }
}

const CollectionOfBoardsPage = () => {
  return (
    <div>
      <Header />
      <BoardsContainer />
    </div>
  )
}

const card = () => {
  return (
    <div>
      <BoardsPage />
      <CardDetails />
    </div>
  )
}



export default App;
